﻿using System;
using System.Reflection;
using System.Resources;

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Retail")]
#endif
[assembly: AssemblyCompany("Vlad Systems")]
[assembly: AssemblyProduct("Rocket")]
[assembly: AssemblyCopyright("2014 Vlad. All rights reserved.")]

[assembly: CLSCompliant(true)]
[assembly: NeutralResourcesLanguage("en-US")]
