﻿using System.Threading.Tasks;

using Digillect.Collections;
using Digillect.Mvvm;

using Rocket.Models;
using Rocket.Services;

namespace Rocket.ViewModels
{
	public class MainViewModel : ViewModel
	{
		private readonly ICashinService _cashinService;

		private XCollection<Atm> _atms;

		#region Constructors/Disposer
		public MainViewModel( ICashinService cashinService )
		{
			_cashinService = cashinService;

			RegisterAction().AddPart( LoadCashin );

		}
		#endregion

		#region Public Properties
		public XCollection<Atm> Atms
		{
			get { return _atms; }
			set { SetProperty( ref _atms, value ); }
		}
		#endregion

		private async Task LoadCashin(Session session)
		{
			var cashin = await _cashinService.GetCashin();

			Atms = cashin.atms;
		}
	}
}
