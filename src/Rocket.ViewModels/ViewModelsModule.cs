﻿using System.Linq;
using System.Reflection;

using Autofac;

using Digillect.Mvvm;

namespace Rocket
{
	public class ViewModelsModule : Autofac.Module
	{
		protected override void Load( ContainerBuilder builder )
		{
			base.Load( builder );

			builder.RegisterAssemblyTypes( GetType().GetTypeInfo().Assembly )
			  .AssignableTo<ViewModel>()
			  .Where( t => !t.GetTypeInfo().IsAbstract && !t.GetTypeInfo().GetCustomAttributes( typeof( SingletonViewModelAttribute ), false ).Any() )
			  .AsSelf()
			  .OwnedByLifetimeScope()
			  .PropertiesAutowired();

			builder.RegisterAssemblyTypes( GetType().GetTypeInfo().Assembly )
			 .AssignableTo<ViewModel>()
			 .Where( t => !t.GetTypeInfo().IsAbstract && t.GetTypeInfo().GetCustomAttributes( typeof( SingletonViewModelAttribute ), false ).Any() )
			 .AsSelf()
			 .OwnedByLifetimeScope()
			 .PropertiesAutowired()
			 .SingleInstance();
		}
	}
}