﻿using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("Rocket.ViewModels")]
[assembly: AssemblyDescription("")]
[assembly: InternalsVisibleTo( "Rocket.UnitTests" )]
[assembly: InternalsVisibleTo( "Rocket.IntegrationTests" )]
