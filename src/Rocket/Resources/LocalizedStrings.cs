﻿namespace Rocket.Resources
{
	public class LocalizedStrings
	{
		public LocalizedStrings()
		{
		}

		private static readonly AppResources localizedResources = new AppResources();

		public AppResources LocalizedResources { get { return localizedResources; } }
	}
}
