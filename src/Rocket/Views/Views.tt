﻿<#@ template debug="true" hostSpecific="true" language="C#" #>
<#@ Assembly Name="System.Core" #>
<#@ assembly name="System.Xml" #>
<#@ assembly name="System.Xml.Linq" #>
<#@ import namespace="System" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ import namespace="System.Diagnostics" #>
<#@ import namespace="System.IO" #>
<#@ import namespace="System.Linq" #>
<#@ import namespace="System.Text" #>
<#@ import namespace="System.Xml.Linq" #>
<#@ output extension=".xml.cs" #>
<#
var content = File.ReadAllText( Path.Combine( Path.GetDirectoryName( this.Host.TemplateFile ), "Views.xml" ) );
#>
//-----------------------------------------------------------------
// WARNING:
//
// This code is automatically generated: do not modify it directly,
// because all changes will be lost when generator runs again.
//-----------------------------------------------------------------
using System;

using Digillect.Mvvm.UI;

<#
var document = XDocument.Parse( content );
var model = document.Element( NS+"Views" );

var usings = from u in model.Elements( NS+"Using" ) select u;

foreach( var u in usings )
{
#>
using <#= u.Attribute( "Namespace" ).Value #>;
<#
}
#>

namespace <#= model.Attribute( "Namespace" ).Value #>
{
<#
foreach( var modelElement in model.Elements( NS+"View" ) )
{
	string viewName = modelElement.Attribute( "Name" ).Value;
	string viewClassName = GetStringAttribute( modelElement, "Class", viewName );
	string viewPath = GetStringAttribute( modelElement, "Path" );
	bool isModeless = GetBooleanAttribute( modelElement, "Modeless" );
	bool authenticationRequired = GetBooleanAttribute( modelElement, "AuthenticationRequired" );
	bool partOfAuthentication = GetBooleanAttribute( modelElement, "PartOfAuthentication" );
	string fittingName = isModeless ? "Page" : viewClassName + "Fitting";
	string viewModelName = GetStringAttribute( modelElement, "Model" ) ?? GetViewModelNameFromViewName( viewClassName );
	string entityName = GetStringAttribute( modelElement, "Entity" );

#>
	#region <#= viewName #>
<#
	if( viewName != viewClassName )
	{
#>
	[View( "<#= viewName #>" )]
<#
	}
	else
	{
#>
	[View]
<#
	}

	if( viewPath != null )
	{
#>
	[ViewPath( "<#= viewPath #>" )]
<#
	}

	if( authenticationRequired )
	{
#>
	[ViewRequiresAuthentication]
<#
	}

	if( partOfAuthentication )
	{
#>
	[ViewIsPartOfAuthenticationFlow]
<#
	}

	foreach( var parameterElement in modelElement.Elements( NS+"Parameter" ) )
	{
		string parameterName = parameterElement.Attribute( "Name" ).Value;
		string parameterType = GetStringAttribute( parameterElement, "Type" );
		bool required = GetBooleanAttribute( parameterElement, "Required", true );
#>
	[ViewParameter( "<#= parameterName #>"<#= parameterType == null ? "" : ", typeof( "+parameterType + " )" #><#= !required ? ", Required=false" : "" #> )]
<#
	}
#>
	public partial class <#= viewClassName #> : <#= fittingName #>
	{
		#region Constructors/Disposer
		public <#= viewClassName #>()
		{
			BeforeInitializeComponent();
			InitializeComponent();
			AfterInitializeComponent();
		}

		partial void BeforeInitializeComponent();
		partial void AfterInitializeComponent();
		#endregion
	}

<#
	if( !isModeless )
	{
		if( entityName != null )
		{
#>
	public class <#= fittingName #> : EntityPage<<#= entityName #>, <#= viewModelName #>>
<#
		}
		else
		{
#>
	public class <#= fittingName #> : ViewModelPage<<#= viewModelName #>>
<#
		}
#>
	{
	}
<#
	}
#>
	#endregion
<#
}
#>
}
<#+
readonly XNamespace NS = "http://schemas.digillect.com/development/mvvm-views";

private string GetViewModelNameFromViewName( string viewName )
{
	return viewName.EndsWith( "View" ) ? viewName + "Model" : viewName + "ViewModel";
}

bool GetBooleanAttribute( XElement element, string name, bool defaultValue = false )
{
	if( element.Attribute( name ) == null )
	{
		return defaultValue;
	}
	
	var value = element.Attribute( name ).Value;
		
	return value == "True" || value == "true" || value == "1";
}

string GetStringAttribute( XElement element, string name, string defaultValue = null )
{
	if( element.Attribute( name ) == null )
	{
		return defaultValue;
	}
	
	return element.Attribute( name ).Value;
}
#>