﻿//-----------------------------------------------------------------
// WARNING:
//
// This code is automatically generated: do not modify it directly,
// because all changes will be lost when generator runs again.
//-----------------------------------------------------------------
using System;

using Digillect.Mvvm.UI;

using Rocket.Models;
using Rocket.ViewModels;

namespace Rocket.Views
{
	#region MainView
	[View]
	public partial class MainView : MainViewFitting
	{
		#region Constructors/Disposer
		public MainView()
		{
			BeforeInitializeComponent();
			InitializeComponent();
			AfterInitializeComponent();
		}

		partial void BeforeInitializeComponent();
		partial void AfterInitializeComponent();
		#endregion
	}

	public class MainViewFitting : ViewModelPage<MainViewModel>
	{
	}
	#endregion
}
