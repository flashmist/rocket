﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows;

using Newtonsoft.Json;

using Rocket.Models;

namespace Rocket.Services
{
	internal class CashinService : ICashinService
	{
		public async Task<Cashin> GetCashin()
		{
			var cashin = await ReadFileFromData( "Cashin.json" );

			return JsonConvert.DeserializeObject<Cashin>( cashin );
		}

		private async Task<string> ReadFileFromData( string fileName )
		{

			var resrouceStream = Application.GetResourceStream( new Uri( "Data/" + fileName, UriKind.Relative ) );

			if( resrouceStream != null )
			{
				var fileStream = resrouceStream.Stream;

				if( fileStream.CanRead )
				{
					var reader = new StreamReader( fileStream );

					return await reader.ReadToEndAsync();
				}

			}

			return string.Empty;
		}
	}
}
