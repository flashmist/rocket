﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Navigation;

using Autofac;

using Digillect.Mvvm.Services;
using Digillect.Mvvm.UI;

using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

using Rocket.Resources;
using Rocket.Services;

namespace Rocket
{
	public partial class App : PhoneApplication
	{
		#region Constructors/Disposer
		public App()
		{
			UnhandledException += Application_UnhandledException;

			InitializeComponent();
			InitializeLanguage();

			if( Debugger.IsAttached )
			{
				PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;
			}
		}
		#endregion

		#region Navigation handling
		protected override void HandleNavigationFailed( NavigationFailedEventArgs e )
		{
			if( Debugger.IsAttached )
			{
				Debugger.Break();
			}
		}
		#endregion

		#region Error reporting
		private void Application_UnhandledException( object sender, ApplicationUnhandledExceptionEventArgs e )
		{
			if( Debugger.IsAttached )
			{
				Debugger.Break();
			}
		}
		#endregion

		#region Language support
		// Initialize the app's font and flow direction as defined in its localized resource strings.
		//
		// To ensure that the font of your application is aligned with its supported languages and that the
		// FlowDirection for each of those languages follows its traditional direction, ResourceLanguage
		// and ResourceFlowDirection should be initialized in each resx file to match these values with that
		// file's culture. For example:
		//
		// AppResources.es-ES.resx
		//    ResourceLanguage's value should be "es-ES"
		//    ResourceFlowDirection's value should be "LeftToRight"
		//
		// AppResources.ar-SA.resx
		//     ResourceLanguage's value should be "ar-SA"
		//     ResourceFlowDirection's value should be "RightToLeft"
		//
		// For more info on localizing Windows Phone apps see http://go.microsoft.com/fwlink/?LinkId=262072.
		//
		private void InitializeLanguage()
		{
			try
			{
				// Set the font to match the display language defined by the
				// ResourceLanguage resource string for each supported language.
				//
				// Fall back to the font of the neutral language if the Display
				// language of the phone is not supported.
				//
				// If a compiler error is hit then ResourceLanguage is missing from
				// the resource file.
				RootFrame.Language = XmlLanguage.GetLanguage( AppResources.ResourceLanguage );

				// Set the FlowDirection of all elements under the root frame based
				// on the ResourceFlowDirection resource string for each
				// supported language.
				//
				// If a compiler error is hit then ResourceFlowDirection is missing from
				// the resource file.
				var flow = (FlowDirection) Enum.Parse( typeof( FlowDirection ), AppResources.ResourceFlowDirection );
				RootFrame.FlowDirection = flow;
			}
			catch
			{
				// If an exception is caught here it is most likely due to either
				// ResourceLangauge not being correctly set to a supported language
				// code or ResourceFlowDirection is set to a value other than LeftToRight
				// or RightToLeft.

				if( Debugger.IsAttached )
				{
					Debugger.Break();
				}

				throw;
			}
		}
		#endregion

		#region IoC
		protected override void RegisterServices( ContainerBuilder builder )
		{
			base.RegisterServices( builder );

			builder.RegisterModule<ModelsModule>();
			builder.RegisterModule<ViewModelsModule>();

			builder.RegisterType<DefaultViewDiscoveryService>().As<IViewDiscoveryService>().SingleInstance();

			builder.RegisterType<CashinService>().As<ICashinService>().SingleInstance();
		}
		#endregion
	}
}