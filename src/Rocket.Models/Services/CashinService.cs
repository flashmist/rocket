﻿using System;
using System.IO;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Rocket.Models;

namespace Rocket.Services
{
	internal class CashinService
	{
		public async Task<Cashin> GetCashin()
		{
			var categories = await ReadFileFromData( "Cashin.json" );

			return JsonConvert.DeserializeObject<Cashin>( categories );
		}

		private async Task<string> ReadFileFromData( string fileName )
		{

			var resrouceStream = Application.GetResourceStream( new Uri( "Data/" + fileName, UriKind.Relative ) );

			if( resrouceStream != null )
			{
				Stream myFileStream = resrouceStream.Stream;

				if( myFileStream.CanRead )
				{
					StreamReader myStreamReader = new StreamReader( myFileStream );

					return await myStreamReader.ReadToEndAsync();
				}

			}

			return string.Empty;
		}
	}
}
