﻿using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("Rocket.Models")]
[assembly: AssemblyDescription("")]
[assembly: InternalsVisibleTo( "Rocket.UnitTests" )]
[assembly: InternalsVisibleTo( "Rocket.IntegrationTests" )]
