﻿<#@ template debug="false" hostspecific="true" language="C#" #>
<#@ assembly name="System" #>
<#@ assembly name="System.Core" #>
<#@ assembly name="System.Xml" #>
<#@ assembly name="System.Xml.Linq" #>
<#@ assembly name="System.Runtime.Serialization" #>
<#@ import namespace="System.IO" #>
<#@ import namespace="System.Linq" #>
<#@ import namespace="System.Text" #>
<#@ import namespace="System.Xml.Linq" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ import namespace="System.Runtime.Serialization" #>
<#@ output extension=".xml.cs" #>
//-----------------------------------------------------------------
// WARNING:
//
// This code is automatically generated: do not modify it directly,
// because all changes will be lost when generator runs again.
//-----------------------------------------------------------------
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Runtime.Serialization;

using Digillect;
using Digillect.Collections;

<#
	var directoryName = Path.GetDirectoryName( Host.TemplateFile );
	var modelFilePath = directoryName == null ? "Models.xml" : Path.Combine( directoryName, "Models.xml" );
	var content = File.ReadAllText( modelFilePath );
	var document = XDocument.Parse( content );
	var model = document.Element( NS + "Model" );

	if( model != null )
	{
		defaultEntitySerializationMode = GetStringAttribute( model, "EntitySerialization" );
		defaultPropertySerializationMode = GetStringAttribute( model, "PropertySerialization" );

		InitializeTypes( model );

		foreach( XElement @using in model.Elements( NS + "Using" ) )
		{
#>
using <#= @using.Attribute( "Namespace" ).Value #>;
<#
		}
#>

namespace <#= model.Attribute( "Namespace" ).Value #>
{
<#
		var entities = from e in model.Elements( NS + "Entity" ) select e;

		ProcessEntities( entities );
	}
#>
}
<#+
	void ProcessEntities( IEnumerable<XElement> entities )
	{
		foreach( var entity in entities )
		{
			var visibility = GetStringAttribute( entity, "Visibility", "public" );
			var entityName = entity.Attribute( "Name" ).Value;
			var serializationAttribute = ConstructEntitySerializationAttribute( entity );
			var otherAttributes = EmitAttributes( entity );

#>
	#region <#= visibility #> class <#= entityName #>
<#+
			if( serializationAttribute != null )
			{
#>
	[<#= serializationAttribute #>]
<#+
			}

			if( otherAttributes != null )
			{
#>
	[<#= otherAttributes #>]
<#+
			}

			var typeArguments = GetStringAttribute( entity, "TypeArguments" );
			var baseTypeName = GetStringAttribute( entity, "BaseType", "XObject" );
			var baseTypeArguments = GetStringAttribute( entity, "BaseTypeArguments" );
			var fullEntityName = entityName + (typeArguments == null ? "" : "<" + typeArguments + ">");
			var fullBaseTypeName = baseTypeName + (baseTypeArguments == null ? "" : "<" + baseTypeArguments + ">");
			var interfaces = GetStringAttribute( entity, "Interfaces" );
#>
	<#= visibility #> partial class <#= fullEntityName #> : <#= fullBaseTypeName #><#+
			if( !string.IsNullOrEmpty( interfaces ) )
			{
				foreach( var i in interfaces.Split( ',' ) )
				{
					#>, <#= i #><#+
				}
			}
#>

	{
		#region Fields
<#+
			var properties = entity.Elements( NS + "Property" ).ToArray();

			foreach( XElement property in properties )
			{
				if( GetBooleanAttribute( property, "CreateStorage", true ) )
				{
#>
		[DebuggerBrowsable( DebuggerBrowsableState.Never )] private <#= ConstructPropertyType( property ) #> <#= FieldNameForPropertyNamed( property.Attribute( "Name" ).Value ) #>;
<#+
				}
			}
#>
		#endregion

		#region Constructors/Disposer
		public <#= entityName #>()
		{
			InitializeEntity();
		}

		partial void InitializeEntity();

		protected override XObject CreateInstanceOfSameType()
		{
			return new <#= entityName #>();
		}
		#endregion

		#region Properties
<#+
			foreach( XElement property in properties )
			{
				var pName = property.Attribute( "Name" ).Value;

				serializationAttribute = ConstructPropertySerializationAttribute( property );
				otherAttributes = EmitAttributes( property );

				if( serializationAttribute != null )
				{
#>
		[<#= serializationAttribute #>]
<#+
				}

				if( otherAttributes != null )
				{
#>
		[<#= otherAttributes #>]
<#+
				}
				
				visibility = GetStringAttribute( property, "Visibility", "public" );
#>
		<#= visibility #> <#= ConstructPropertyType( property ) #> <#= pName #>
		{
<#+
				if( property.Element( NS+"Code" ) != null )
				{
#>
			<#= property.Element( NS+"Code" ).Value #>
<#+
				}
				else
				{
#>
			get { return <#= FieldNameForPropertyNamed( pName ) #>; }
<#+
					if( !GetBooleanAttribute( property, "ReadOnly" ) )
					{
#>
			set { SetProperty( ref <#= FieldNameForPropertyNamed( pName ) #>, value ); }
<#+
					}
				}
#>
		}
<#+
			}
#>
		#endregion

		#region ProcessCopy
		protected override void ProcessCopy( XObject source, bool cloning, bool deepCloning )
		{
			base.ProcessCopy( source, cloning, deepCloning );

			var other = (<#= fullEntityName #>) source;

			BeforeProcessCopy( other, cloning, deepCloning );

<#+
			var complex = new List<XElement>();
			var sequence = new List<XElement>();

			foreach( XElement property in properties )
			{
				if( !GetBooleanAttribute( property, "CreateStorage", true ) )
				{
					continue;
				}

				var fieldName = FieldNameForPropertyNamed( property.Attribute( "Name" ).Value );
				var updateStrategy = GetUpdateStrategy( property );

				if( updateStrategy == UpdateStrategy.Sequence )
				{
					sequence.Add( property );
				}
				else if( updateStrategy == UpdateStrategy.Complex )
				{
					complex.Add( property );
				}
				else if( updateStrategy == UpdateStrategy.Assign )
				{
#>
			<#= fieldName #> = other.<#= fieldName #>;
<#+
				}
			}

			if( complex.Count > 0 )
			{
#>

			if( cloning )
			{
<#+
				foreach( var property in complex )
				{
					var fieldName = FieldNameForPropertyNamed( property.Attribute( "Name" ).Value );
#>
				<#= fieldName #> = other.<#= fieldName #> == null ? null : (<#= ConstructPropertyType( property ) #>) other.<#= fieldName #>.Clone( deepCloning );
<#+
				}
#>
			}
			else
			{
<#+
				foreach( var property in complex )
				{
					var fieldName = FieldNameForPropertyNamed( property.Attribute( "Name" ).Value );
#>

				if( <#= fieldName #> != null )
				{
					if( other.<#= fieldName #> != null )
					{
						<#= fieldName #>.Update( other.<#= fieldName #> );
					}
					else
					{
						<#= fieldName #> = null;
					}
				}
				else
				{
					if( other.<#= fieldName #> != null )
					{
						<#= fieldName #> = (<#= ConstructPropertyType( property ) #>) other.<#= fieldName #>.Clone( false );
					}
				}
<#+
				}
#>
			}
<#+
			}

			if( sequence.Count > 0 )
			{
				foreach( var property in sequence )
				{
					var fieldName = FieldNameForPropertyNamed( property.Attribute( "Name" ).Value );
#>

			if( cloning && other.<#= fieldName #> != null )
			{
				<#= fieldName #> = new <#= ConstructPropertyType( property ) #>();

				foreach( var item in other.<#= fieldName #> )
				{
					<#= fieldName #>.Add( item );
				}
			}
			else
			{
				<#= fieldName #> = other.<#= fieldName #>;
			}
<#+
				}
			}
#>

			AfterProcessCopy( other, cloning, deepCloning );
		}

		partial void BeforeProcessCopy( <#= entityName #> source, bool cloning, bool deepCloning );
		partial void AfterProcessCopy( <#= entityName #> source, bool cloning, bool deepCloning );
		#endregion
<#+			
			if( entity.Attribute( "XKeyProperties" ) != null && !string.IsNullOrWhiteSpace( entity.Attribute( "XKeyProperties" ).Value ) )
			{
#>
		#region CreateKey
		protected override XKey CreateKey()
		{
			XKey key = base.CreateKey();
			
<#+
				var names = GetStringAttribute( entity, "XKeyProperties" ).Split( ',' );
				
				foreach( var pName in names )
				{
#>
			key = key.WithKey( "<#= pName #>", <#= FieldNameForPropertyNamed( pName ) #> );
<#+
				}
#>

			return key;
		}
		#endregion
<#+
			}

			if( GetBooleanAttribute( entity, "GenerateEquals", true ) )
			{
#>
		#region Equals/GetHashCode
		public override bool Equals( object otherObject )
		{
<#+
				TypeDescriptor baseTypeDescriptor;
				
				types.TryGetValue( baseTypeName, out baseTypeDescriptor );

				if( (baseTypeDescriptor != null && baseTypeDescriptor.Kind == TypeKind.XObject) || (baseTypeName == "XObject" && baseTypeArguments != null) )
				{
#>
			if( !base.Equals( otherObject ) )
			{
				return false;
			}

<#+
				}
#>
			var other = otherObject as <#= fullEntityName #>;

			if( other == null )
			{
				return false;
			}
<#+
				foreach( XElement property in properties )
				{
					var fieldName = FieldNameForPropertyNamed( property.Attribute( "Name" ).Value );

					if( !GetBooleanAttribute( property, "CreateStorage", true ) )
					{
						continue;
					}

					if( IsSequenceTypeProperty( property ) )
					{
#>

			if( <#= fieldName #> == null && other.<#= fieldName #> != null )
			{
				return false;
			}

			if( <#= fieldName #> != null && other.<#= fieldName #> == null )
			{
				return false;
			}

			if( <#= fieldName #> != null )
			{
				if( !<#= fieldName #>.SequenceEqual( other.<#= fieldName #> ) )
				{
					return false;
				}
			}
<#+
					}
					else
					{
#>

			if( !Equals( <#= fieldName #>, other.<#= fieldName #> ) )
			{
				return false;
			}
<#+
					}
				}
#>

			return true;
		}

		public override int GetHashCode()
		{
<#+
				types.TryGetValue( baseTypeName, out baseTypeDescriptor );

				if( (baseTypeDescriptor != null && baseTypeDescriptor.Kind == TypeKind.XObject) || (baseTypeName == "XObject" && baseTypeArguments != null) )
				{
#>
			int hashCode = base.GetHashCode();
<#+	
				}
				else
				{
#>
			int hashCode = 17;
<#+
				}

				foreach( XElement property in properties )
				{
					string pName = property.Attribute( "Name" ).Value;
					var fieldName = FieldNameForPropertyNamed( pName );

					if( !GetBooleanAttribute( property, "CreateStorage", true ) )
					{
						continue;
					}

					if( IsValueTypeProperty( property ) )
					{
#>

			hashCode = hashCode * 37 + <#= fieldName #>.GetHashCode();
<#+
					}
					else
					{
#>

			if( <#= fieldName #> != null )
			{
				hashCode = hashCode * 37 + <#= fieldName #>.GetHashCode();
			}
<#+
					}
				}
#>

			return hashCode;
		}
		#endregion
<#+
			}

			if( entity.Element( NS+"Code" ) != null )
			{
#>
		#region Custom Code
<#= entity.Element( NS+"Code" ).Value #>
		#endregion
<#+
			}
			
			var innerEntities = from e in entity.Elements( NS+"Entity" ) select e;

			PushIndent( "\t" );
			ProcessEntities( innerEntities );
			PopIndent();
#>
	}
	#endregion
<#+
		}
	}
#>
<#+
	enum UpdateStrategy
	{
		Ignore,
		Assign,
		Sequence,
		Complex,
	}

	enum TypeKind
	{
		Value,
		Object,
		XObject,
		Sequence,
	}

	readonly XNamespace NS = "http://schemas.digillect.com/development/datamodel";
	readonly Dictionary<string, TypeDescriptor> types = new Dictionary<string, TypeDescriptor>();

	string defaultEntitySerializationMode;
	string defaultPropertySerializationMode;

	private class TypeDescriptor
	{
		public string Name { get; private set; }
		public string FullName { get; private set; }
		public TypeKind Kind { get; private set; }

		public TypeDescriptor( XElement source )
		{
			Name = source.Attribute( "Name" ).Value;
			FullName = source.Attribute( "FullName" ) == null ? Name : (string) source.Attribute( "FullName" ).Value;
			Kind = (TypeKind) Enum.Parse( typeof( TypeKind ), source.Attribute( "Kind" ).Value, true );
		}

		public TypeDescriptor( string name, TypeKind kind )
			: this( name, name, kind )
		{
		}

		public TypeDescriptor( string name, string fullName, TypeKind kind )
		{
			Name = name;
			FullName = fullName;
			Kind = kind;
		}
	}

	void InitializeTypes( XElement model )
	{
		types.Add( "bool", new TypeDescriptor( "bool", TypeKind.Value ) );
		types.Add( "char", new TypeDescriptor( "char", TypeKind.Value ) );
		types.Add( "int", new TypeDescriptor( "int", TypeKind.Value ) );
		types.Add( "uint", new TypeDescriptor( "uint", TypeKind.Value ) );
		types.Add( "long", new TypeDescriptor( "long", TypeKind.Value ) );
		types.Add( "ulong", new TypeDescriptor( "ulong", TypeKind.Value ) );
		types.Add( "float", new TypeDescriptor( "float", TypeKind.Value ) );
		types.Add( "double", new TypeDescriptor( "double", TypeKind.Value ) );
		types.Add( "DateTime", new TypeDescriptor( "DateTime", TypeKind.Value ) );
		types.Add( "TimeSpan", new TypeDescriptor( "TimeSpan", TypeKind.Value ) );
		types.Add( "Guid", new TypeDescriptor( "Guid", TypeKind.Value ) );

		types.Add( "string", new TypeDescriptor( "string", TypeKind.Object ) );
		types.Add( "ObservableCollection", new TypeDescriptor( "ObservableCollection", TypeKind.Sequence ) );

		var externals = model.Element( NS + "Externals" );

		if( externals != null )
		{
			foreach( var externalType in externals.Elements( NS + "Type" ) )
			{
				var descriptor = new TypeDescriptor( externalType );

				if( !types.ContainsKey( descriptor.Name ) )
				{
					types.Add( descriptor.Name, descriptor );
				}
			}
		}

		var entities = from e in model.Elements( NS+"Entity" ) select e;

		BuildEntityTypes( entities );
	}
	
	void BuildEntityTypes( IEnumerable<XElement> entities )
	{
		foreach( var entity in entities )
		{
			var name = entity.Attribute( "Name" ).Value;

			if( !types.ContainsKey( name ) )
			{
				types.Add( name, new TypeDescriptor( name, TypeKind.XObject ) );
			}

			var innerEntities = from e in entity.Elements( NS+"Entity" ) select e;

			BuildEntityTypes( innerEntities );
		}
	}

	UpdateStrategy GetUpdateStrategy( XElement property )
	{
		var updateMode = GetStringAttribute( property, "Update", "auto" );
		
		if( updateMode == "ignore" )
		{
			return UpdateStrategy.Ignore;
		}

		if( updateMode == "assign" )
		{
			return UpdateStrategy.Assign;
		}

		var typeName = GetStringAttribute( property, "Type" );
		TypeDescriptor descriptor;

		if( typeName.EndsWith( "[]" ) || typeName.EndsWith( "?" ) )
		{
			return UpdateStrategy.Assign;
		}

		if( types.TryGetValue( typeName, out descriptor ) )
		{
			if( descriptor.Kind == TypeKind.Sequence )
			{
				return UpdateStrategy.Sequence;
			}

			if( descriptor.Kind != TypeKind.XObject )
			{
				return UpdateStrategy.Assign;
			}
		}

		return UpdateStrategy.Complex;
	}

	bool IsValueTypeProperty( XElement property )
	{
		var typeName = GetStringAttribute( property, "Type" );
		TypeDescriptor descriptor;

		return types.TryGetValue( typeName, out descriptor ) && descriptor.Kind == TypeKind.Value;
	}

	bool IsSequenceTypeProperty( XElement property )
	{
		var typeName = GetStringAttribute( property, "Type" );
		TypeDescriptor descriptor;

		return typeName.EndsWith( "[]" ) || (types.TryGetValue( typeName, out descriptor ) && descriptor.Kind == TypeKind.Sequence);
	}

	string ConstructPropertyType( XElement property )
	{
		var typeName = property.Attribute( "Type" ).Value;
		var genericArgs = property.Attribute( "TypeArguments" );

		if( genericArgs == null )
		{
			return typeName;
		}

		return string.Format( "{0}<{1}>", typeName, genericArgs.Value );
	}

	string EmitAttributes( XElement element )
	{
		StringBuilder sb = null;
		
		foreach( XElement a in element.Elements( NS + "Attribute" ) )
		{
			if( sb == null )
			{
				sb = new StringBuilder();
			}
			else
			{
				sb.Append( ", " );
			}

			if( a.Attribute( "Parameters" ) != null )
			{
				sb.Append( (string) a.Attribute( "Name" ).Value )
					.Append( "( " )
					.Append( (string) a.Attribute( "Parameters" ).Value )
					.Append( " )" );
			}
			else
			{
				sb.Append( (string) a.Attribute( "Name" ).Value );
			}
		}
		
		return sb == null ? null : sb.ToString();
	}

	string ConstructEntitySerializationAttribute( XElement entity )
	{
		var serializationMode = GetStringAttribute( entity, "Serialization", defaultEntitySerializationMode ?? "contract" );
		var serializationName = GetStringAttribute( entity, "SerializationName" );

		string serializationAttributeName = null, serializationAttributeParameters = null;

		switch( serializationMode )
		{
			case "xml":
				serializationAttributeName = "XmlRoot";

				if( serializationName != null )
				{
					var parts = serializationName.Split( ',' );

					if( parts.Length == 1 )
					{
						serializationAttributeParameters = "\"" + serializationName + "\"";
					}
					else
					{
						serializationAttributeParameters = string.Format( "\"{1}\", Namespace=\"{0}\"", parts[0], parts[1] );
					}
				}

				break;

			case "none":
				break;

			default:
				serializationAttributeName = "DataContract";

				if( serializationName != null )
				{
					serializationAttributeParameters = "Name = \"" + serializationName + "\"";
				}

				break;
		}

		if( serializationAttributeName != null )
		{
			if( serializationAttributeParameters != null )
			{
				return serializationAttributeName + "( " + serializationAttributeParameters + ")";
			}

			return serializationAttributeName;
		}

		return null;
	}

	string ConstructPropertySerializationAttribute( XElement property )
	{
		var serializationMode = GetStringAttribute( property, "Serialization", defaultPropertySerializationMode ?? "contract" );
		var serializationName = GetStringAttribute( property, "SerializationName" );

		string serializationAttributeName = null;
		string serializationAttributeParameters = null;

		switch( serializationMode )
		{
			case "element":
				serializationAttributeName = "XmlElement";

				if( serializationName != null )
				{
					var parts = serializationName.Split( ',' );

					if( parts.Length == 1 )
					{
						serializationAttributeParameters = "\"" + serializationName + "\"";
					}
					else
					{
						serializationAttributeParameters = string.Format( "\"{1}\", Namespace=\"{0}\"", parts[0], parts[1] );
					}
				}

				break;

			case "attribute":
				serializationAttributeName = "XmlAttribute";

				if( serializationName != null )
				{
					var parts = serializationName.Split( ',' );

					if( parts.Length == 1 )
					{
						serializationAttributeParameters = "\"" + serializationName + "\"";
					}
					else
					{
						serializationAttributeParameters = string.Format( "\"{1}\", Namespace=\"{0}\"", parts[0], parts[1] );
					}
				}

				break;

			case "none":
				break;

			default:
				serializationAttributeName = "DataMember";

				if( serializationName != null )
				{
					serializationAttributeParameters = "Name = \"" + serializationName + "\"";
				}

				break;
		}

		if( serializationAttributeName != null )
		{
			if( serializationAttributeParameters != null )
			{
				return serializationAttributeName + "( " + serializationAttributeParameters + ")";
			}

			return serializationAttributeName;
		}

		return null;
	}

	bool GetBooleanAttribute( XElement element, string name, bool defaultValue = false )
	{
		if( element.Attribute( name ) == null )
		{
			return defaultValue;
		}

		var value = element.Attribute( name ).Value;
		
		return value == "True" || value == "true" || value == "1";
	}

	string GetStringAttribute( XElement element, string name, string defaultValue = null )
	{
		if( element.Attribute( name ) != null )
		{
			return element.Attribute( name ).Value;
		}

		return defaultValue;
	}

	string FieldNameForPropertyNamed( string propertyName )
	{
		return "_" + char.ToLower( propertyName[0] ) + propertyName.Substring( 1 );
	}
#>