﻿//-----------------------------------------------------------------
// WARNING:
//
// This code is automatically generated: do not modify it directly,
// because all changes will be lost when generator runs again.
//-----------------------------------------------------------------
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Runtime.Serialization;

using Digillect;
using Digillect.Collections;


namespace Rocket.Models
{
	#region public class Cashin
	[DataContract]
	public partial class Cashin : XObject
	{
		#region Fields
		[DebuggerBrowsable( DebuggerBrowsableState.Never )] private XCollection<Atm> _atms;
		#endregion

		#region Constructors/Disposer
		public Cashin()
		{
			InitializeEntity();
		}

		partial void InitializeEntity();

		protected override XObject CreateInstanceOfSameType()
		{
			return new Cashin();
		}
		#endregion

		#region Properties
		[DataMember]
		public XCollection<Atm> atms
		{
			get { return _atms; }
			set { SetProperty( ref _atms, value ); }
		}
		#endregion

		#region ProcessCopy
		protected override void ProcessCopy( XObject source, bool cloning, bool deepCloning )
		{
			base.ProcessCopy( source, cloning, deepCloning );

			var other = (Cashin) source;

			BeforeProcessCopy( other, cloning, deepCloning );


			if( cloning )
			{
				_atms = other._atms == null ? null : (XCollection<Atm>) other._atms.Clone( deepCloning );
			}
			else
			{

				if( _atms != null )
				{
					if( other._atms != null )
					{
						_atms.Update( other._atms );
					}
					else
					{
						_atms = null;
					}
				}
				else
				{
					if( other._atms != null )
					{
						_atms = (XCollection<Atm>) other._atms.Clone( false );
					}
				}
			}

			AfterProcessCopy( other, cloning, deepCloning );
		}

		partial void BeforeProcessCopy( Cashin source, bool cloning, bool deepCloning );
		partial void AfterProcessCopy( Cashin source, bool cloning, bool deepCloning );
		#endregion
		#region Equals/GetHashCode
		public override bool Equals( object otherObject )
		{
			var other = otherObject as Cashin;

			if( other == null )
			{
				return false;
			}

			if( !Equals( _atms, other._atms ) )
			{
				return false;
			}

			return true;
		}

		public override int GetHashCode()
		{
			int hashCode = 17;

			if( _atms != null )
			{
				hashCode = hashCode * 37 + _atms.GetHashCode();
			}

			return hashCode;
		}
		#endregion
	}
	#endregion
	#region public class Atm
	[DataContract]
	public partial class Atm : XObject
	{
		#region Fields
		[DebuggerBrowsable( DebuggerBrowsableState.Never )] private int _id;
		[DebuggerBrowsable( DebuggerBrowsableState.Never )] private string _name;
		[DebuggerBrowsable( DebuggerBrowsableState.Never )] private bool _rur;
		[DebuggerBrowsable( DebuggerBrowsableState.Never )] private bool _usd;
		[DebuggerBrowsable( DebuggerBrowsableState.Never )] private bool _eur;
		[DebuggerBrowsable( DebuggerBrowsableState.Never )] private string _hours;
		[DebuggerBrowsable( DebuggerBrowsableState.Never )] private string _address;
		[DebuggerBrowsable( DebuggerBrowsableState.Never )] private double _latitude;
		[DebuggerBrowsable( DebuggerBrowsableState.Never )] private double _longitude;
		[DebuggerBrowsable( DebuggerBrowsableState.Never )] private bool _hidden;
		[DebuggerBrowsable( DebuggerBrowsableState.Never )] private string _type;
		#endregion

		#region Constructors/Disposer
		public Atm()
		{
			InitializeEntity();
		}

		partial void InitializeEntity();

		protected override XObject CreateInstanceOfSameType()
		{
			return new Atm();
		}
		#endregion

		#region Properties
		[DataMember]
		public int Id
		{
			get { return _id; }
			set { SetProperty( ref _id, value ); }
		}
		[DataMember]
		public string Name
		{
			get { return _name; }
			set { SetProperty( ref _name, value ); }
		}
		[DataMember]
		public bool Rur
		{
			get { return _rur; }
			set { SetProperty( ref _rur, value ); }
		}
		[DataMember]
		public bool Usd
		{
			get { return _usd; }
			set { SetProperty( ref _usd, value ); }
		}
		[DataMember]
		public bool Eur
		{
			get { return _eur; }
			set { SetProperty( ref _eur, value ); }
		}
		[DataMember]
		public string Hours
		{
			get { return _hours; }
			set { SetProperty( ref _hours, value ); }
		}
		[DataMember]
		public string Address
		{
			get { return _address; }
			set { SetProperty( ref _address, value ); }
		}
		[DataMember( Name = "lat")]
		public double Latitude
		{
			get { return _latitude; }
			set { SetProperty( ref _latitude, value ); }
		}
		[DataMember( Name = "lon")]
		public double Longitude
		{
			get { return _longitude; }
			set { SetProperty( ref _longitude, value ); }
		}
		[DataMember]
		public bool Hidden
		{
			get { return _hidden; }
			set { SetProperty( ref _hidden, value ); }
		}
		[DataMember]
		public string Type
		{
			get { return _type; }
			set { SetProperty( ref _type, value ); }
		}
		#endregion

		#region ProcessCopy
		protected override void ProcessCopy( XObject source, bool cloning, bool deepCloning )
		{
			base.ProcessCopy( source, cloning, deepCloning );

			var other = (Atm) source;

			BeforeProcessCopy( other, cloning, deepCloning );

			_id = other._id;
			_name = other._name;
			_rur = other._rur;
			_usd = other._usd;
			_eur = other._eur;
			_hours = other._hours;
			_address = other._address;
			_latitude = other._latitude;
			_longitude = other._longitude;
			_hidden = other._hidden;
			_type = other._type;

			AfterProcessCopy( other, cloning, deepCloning );
		}

		partial void BeforeProcessCopy( Atm source, bool cloning, bool deepCloning );
		partial void AfterProcessCopy( Atm source, bool cloning, bool deepCloning );
		#endregion
		#region Equals/GetHashCode
		public override bool Equals( object otherObject )
		{
			var other = otherObject as Atm;

			if( other == null )
			{
				return false;
			}

			if( !Equals( _id, other._id ) )
			{
				return false;
			}

			if( !Equals( _name, other._name ) )
			{
				return false;
			}

			if( !Equals( _rur, other._rur ) )
			{
				return false;
			}

			if( !Equals( _usd, other._usd ) )
			{
				return false;
			}

			if( !Equals( _eur, other._eur ) )
			{
				return false;
			}

			if( !Equals( _hours, other._hours ) )
			{
				return false;
			}

			if( !Equals( _address, other._address ) )
			{
				return false;
			}

			if( !Equals( _latitude, other._latitude ) )
			{
				return false;
			}

			if( !Equals( _longitude, other._longitude ) )
			{
				return false;
			}

			if( !Equals( _hidden, other._hidden ) )
			{
				return false;
			}

			if( !Equals( _type, other._type ) )
			{
				return false;
			}

			return true;
		}

		public override int GetHashCode()
		{
			int hashCode = 17;

			hashCode = hashCode * 37 + _id.GetHashCode();

			if( _name != null )
			{
				hashCode = hashCode * 37 + _name.GetHashCode();
			}

			hashCode = hashCode * 37 + _rur.GetHashCode();

			hashCode = hashCode * 37 + _usd.GetHashCode();

			hashCode = hashCode * 37 + _eur.GetHashCode();

			if( _hours != null )
			{
				hashCode = hashCode * 37 + _hours.GetHashCode();
			}

			if( _address != null )
			{
				hashCode = hashCode * 37 + _address.GetHashCode();
			}

			hashCode = hashCode * 37 + _latitude.GetHashCode();

			hashCode = hashCode * 37 + _longitude.GetHashCode();

			hashCode = hashCode * 37 + _hidden.GetHashCode();

			if( _type != null )
			{
				hashCode = hashCode * 37 + _type.GetHashCode();
			}

			return hashCode;
		}
		#endregion
	}
	#endregion
}
